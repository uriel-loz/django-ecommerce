from rest_framework import generics;
from rest_framework.response import Response
from rest_framework import viewsets

from apps.base.api import GeneralListApiView
from apps.products.api.serializers.product_serializers import ProductSerializer

class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer

    def get_queryset(self, pk = None):
        if pk is None:
            instance = self.get_serializer().Meta.model.objects.filter(state = True)
        else:
            instance = self.get_serializer().Meta.model.objects.filter(state = True, id = pk).first()

        return instance

    def list(self, request):
        serializer = self.serializer_class( self.get_queryset(), many = True )

        output = {
            'code':  200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': serializer.data 
        }

        return Response(output)

    def create(self, request):
        serializer = self.serializer_class(data = request.data)
        
        if serializer.is_valid():
            serializer.save()
            
            output = {
                'code':  200,
                'status': 'success',
                'message': 'Operation Successfull'  
            }
        else: 
            output = {
                'code': 0,
                'status':   'error',
                'message': serializer.errors
            }
            
        return Response(output)

    def retrieve(self, request, pk = None):
        serializer = self.serializer_class( self.get_queryset(pk) )
        
        if serializer:
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull',
                'data': serializer.data
            }
        else:
            output = {
                'code': 0,
                'status': 'error',
                'message': 'Product not found'
            }
        
        return Response( output )

    def update(self, request, pk = None):
        if self.get_queryset(pk):
            product_serializer = self.serializer_class( self.get_queryset(pk), data = request.data )
            
            if product_serializer.is_valid():
                product_serializer.save()

                output = {
                    'code': 200,
                    'status': 'success',
                    'message': 'Operation Successfull',
                    'data': product_serializer.data
                }
            else:
                output = {
                    'code': 0,
                    'status': 'error',
                    'message': product_serializer.errors,
                }
        else:
            output = {
                'code': 0,
                'status': 'error',
                'message': 'Product not found'
            }
        
        return Response( output )

    def destroy(self, request,pk=None):
        product = self.get_queryset.filter(id = pk).first()
        
        if product:
            product.state = False
            product.save()
            
            output = {
                'code': 200,
                'status':   'success',
                'message': 'Operation Successfull'
            }
        else:
            output = {
                'code': 0,
                'status': 'error',
                'message': 'No se encontro el producto'
            }

        return Response( output )