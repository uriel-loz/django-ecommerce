from rest_framework import serializers

from apps.products.models import Product
from apps.products.api.serializers.general_serializers import MeasureUnitSerializer, CategoryProductSerializer

class ProductSerializer(serializers.ModelSerializer):
    # 1 Forma para serializar relaciones con sus modelos a traves de su serializador
    # measure_unit_id = MeasureUnitSerializer()
    # category_product_id = CategoryProductSerializer()
    
    # 2 Forma para serializar relaciones con sus modelos a traves de la definción __str__ del modelo
    # que hace referencia
    # measure_unit_id = serializers.StringRelatedField()
    # category_product_id = serializers.StringRelatedField()
    
    class Meta:
        model = Product
        exclude = ('state', 'deleted_at')
        
    # 3 Forma de serializar relaciones con sus modelos a traves de to_representation y llamando
    # a la propiedad de la instancia que se hace referencia al modelo    
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'image': instance.image if instance.image else '',
            'measure_unit': instance.measure_unit_id.description,
            'category_product': instance.category_product_id.description,
            'measure_unit_id': instance.measure_unit_id.id,
            'category_product_id': instance.category_product_id.id
        }