from rest_framework import serializers
from apps.users.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        
    def create( self, validated_data ):
        user = User( **validated_data )
        user.set_password( validated_data[ 'password' ] )
        user.save()
        
        return user
    
    def update( self, instance, validated_data ):
        user_update = super().update( instance, validated_data )
        user_update.set_password( validated_data[ 'password' ] )
        user_update.save()
       
class UserSerializerList(serializers.ModelSerializer):
    class Meta:
        model = User
        
    # Va a funcionar cuando mostramos detalles o un listado de instancias pero tenemos que especificar los campos en la vista
    # como en un select en ese caso accedemos de hacerlo los valores estan en un diccionario y se tendran que acceder con []
    
    # Si no especificamos los campos tendremos que acceder a un objeto de la instancia y como tal es con .
    def to_representation(self, instance):
        return {
           'id': instance.id,
           'username': instance.username,
           'email': instance.email,
           'password': instance.password
        }