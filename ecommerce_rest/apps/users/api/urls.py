from django.urls import path
from apps.users.api.api import UserAPIView, UserAPIViewDetail
# from apps.users.api.api import user_api_view

urlpatterns = [
    path( '', UserAPIView.as_view(), name = 'user_api' ),
    path( '<int:pk>/', UserAPIViewDetail.as_view(), name = 'user_api_detail' )
    # path( 'v1/users/', user_api_view, name = 'user_api' )
]
