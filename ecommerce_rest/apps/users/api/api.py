from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404
from apps.users.models import User
from apps.users.api.serializers import UserSerializer, UserSerializerList

class UserAPIView(APIView):
    def get(self, request):
        users = User.objects.all()
        serializer_class = UserSerializerList(users, many = True)
        
        output = {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': serializer_class.data
        }
        
        return Response(output, status = status.HTTP_200_OK)
    
    def post( self, request ):
        serializer_class = UserSerializer(data = request.data)
        
        if serializer_class.is_valid():
            serializer_class.save()
            
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull'
            }
            
            response = status.HTTP_200_OK;
        else:
            output = {
                'code': 0,
                'status': 'error',
                'message': serializer_class.errors
            }
            
            response = status.HTTP_400_BAD_REQUEST;
            
        return Response( output, status = response )
    
class UserAPIViewDetail(APIView):
        
    def get(self, request, pk):        
        user = get_object_or_404( User, pk = pk )    
        serializer_class = UserSerializerList(user)
        
        output = {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': serializer_class.data
        }
        
        return Response(output, status = status.HTTP_200_OK)
    
    def put( self, request, pk ):
        user = get_object_or_404( User, pk = pk )    
        serializer_class = UserSerializer(user, data = request.data)
        
        if serializer_class.is_valid():
            serializer_class.save()
            
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull'
            }
            
            response = status.HTTP_200_OK;
        else:
            output = {
                'code': 0,
                'status': 'error',
                'message': serializer_class.errors
            }
            
            response = status.HTTP_400_BAD_REQUEST;
            
        return Response( output, status = response )
    
    def delete( self, request, pk ):
        user = get_object_or_404( User, pk = pk )    
        user.delete()
        
        output = {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull'
        }
        
        return Response( output, status = status.HTTP_200_OK )
        

# @api_view(['GET', 'POST'])
# def user_api_view(request):
    
#     if request.method == 'GET':
#         users = User.objects.all()
#         serializer_class = UserSerializer(users, many = True)
        
#         output = {
#             'code': 200,
#             'status': 'success',
#             'message': 'Operation Successfull',
#             'data': serializer_class.data
#         }
        
#         return Response(output)
#     elif request.method == 'POST':
#         print(request.data)