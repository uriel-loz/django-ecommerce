from django.contrib import admin
from django.urls import path
from django.urls import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include( [
        path('users/', include( 'apps.users.api.urls' ) ),
        path('products/', include( 'apps.products.api.routers' ) ),
    ] ) )
    # path( 'api/', include( 'apps.users.api.urls' ) ),
    # path( 'api/', include( 'apps.products.api.urls' ) )
]
